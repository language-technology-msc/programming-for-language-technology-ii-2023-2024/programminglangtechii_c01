import nltk
# https://www.demo2s.com/python/python-importing-from-a-module-using-alias-and-short-name.html
# from <module_name> import <element> as <alias>
from nltk.stem import WordNetLemmatizer as WN

wordnet_lemmatizer = WN()

# example texts to lemmatize
sentence = "The Biden administration has moved swiftly to distance itself from Trump-era immigration policies, but at the US-Mexico border, a pandemic-related order put in place by former President Donald Trump remains in effect, allowing officials to continue to turn away thousands of migrants."

#sentence = "He was running and eating at same time. He has bad habit of swimming after playing long hours in the Sun."


# we don't want lemmas for punctuations
punctuations="?:!.,;"
# get tokens
sentence_tokens = nltk.word_tokenize(sentence)
for word in sentence_tokens:
    # remove punctuations
    if word in punctuations:
        sentence_tokens.remove(word)

#sentence_tokens
# print results
print("{0:20}{1:20}".format("Word","Lemma"))
print('-----------------------------------')
for word in sentence_tokens:
    print ("{0:20}{1:20}".format(word,wordnet_lemmatizer.lemmatize(word)))