import io


def read_contents_of_a_file(filepath):
    """
    reads file contents
    """
    file = open(filepath, 'r')
    content = file.read()
    print(content)
    return content


def write_result(filepath, fdist_most_common):
    """
    writes fdist_most_common array to file with name filepath
    """
    result = open(filepath, 'w')
    # list to string
    list_to_str = ' '.join([str(feat) for feat in fdist_most_common])
    result.write(list_to_str)
    result.close()
