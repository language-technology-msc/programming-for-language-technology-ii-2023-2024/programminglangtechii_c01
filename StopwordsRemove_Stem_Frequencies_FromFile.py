import io
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist

from StopwordsRemove import filter_stopwords
from FileUtils import read_contents_of_a_file, write_result
from nltk.stem import PorterStemmer

# load stopwords
stop_words = set(stopwords.words('english'))

# read file contents
review = read_contents_of_a_file('./data/review.txt')
print(review)

# filter review
review_filtered_stopwords = filter_stopwords(review, stop_words)
print(review_filtered_stopwords)

# open output file (w mode)
result = open('data/review_filter_stopwords.txt', 'w')
for r in review_filtered_stopwords:
    result.write(" " + r)
result.close()

# calculate distributions/frequencies
fdist = FreqDist(review_filtered_stopwords)
fdist_most_common = fdist.most_common(30)
# write results
write_result('data/review_filter_stopwords_dist.txt', fdist_most_common)

# Now let's do stemming
# Use porter stemmer
porter = PorterStemmer()

# function that stems an array of tokens
def stem_tokens(tokens):
    stemmed_tokens = []
    for tok in tokens:
        stemmed_tokens.append(porter.stem(tok))

    return stemmed_tokens;


stemmed_tokens = stem_tokens(review_filtered_stopwords)
# calculate distributions/frequencies for stemmed
fdist = FreqDist(stemmed_tokens)
fdist_most_common = fdist.most_common(30)
# write results
write_result('data/review_filter_stopwords_stem_dist.txt', fdist_most_common)
