# First import from the tokenizer package ('tokenize') of ntlk library the sent_tokenize and word_tokenize.
from nltk.tokenize import sent_tokenize, word_tokenize

# tokenize some text (see MyFirstTokenizer.py)
string_to_tokenize = 'This is the first lesson of Language Technology II! It is Wednesday and the date is 19-5-2021. '
print(word_tokenize(string_to_tokenize))

# tokenize some text (see MyFirstTokenizer.py)
string_to_tokenize = 'I have a Ph.D. I want to work for AT&T. A ticket train costs $30. '
print(word_tokenize(string_to_tokenize))

# get sentences sentences
print(sent_tokenize(string_to_tokenize))



